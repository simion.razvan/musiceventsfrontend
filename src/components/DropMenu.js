import React from "react";
import { Button } from "./Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import { Link } from "react-router-dom";
import "../App.css";
import "./NavBar.css";
import AuthOptions from "./auth/AuthOptions";

export default function DropMenu() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className="drop-menu">
      <Button
        className="btn"
        buttonStyle="btn--outline"
        buttonSize="btn--small"
        onClick={handleClick}
      >
        Menu
      </Button>
      <Menu 
        className="drop--menu--list"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {/* <Link 
            to='/login'>
          <MenuItem onClick={handleClose}>Login</MenuItem>
        </Link>
        <Link to='/signup'>
          <MenuItem onClick={handleClose}>Register</MenuItem>
        </Link> */}
          <AuthOptions onClick={handleClose}></AuthOptions>
      </Menu>
    </div>
  );
}
