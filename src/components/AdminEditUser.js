import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Navbar from "./AdminNavBar";
import Checkbox from "@material-ui/core/Checkbox";
import jwt from "jwt-decode";
import { FormControlLabel } from "@material-ui/core";
import FormGroup from "@material-ui/core/FormGroup";
import UserService from "../services/UserService";


export default class AdminEditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: this.props.match.params.id,
      token: '',
      firstName: '',
      lastName: '',
      email: '',
      roles: '',
      checkbox: [
        {
          checkedAdmin: true,
          checkedUser: true,
          checkedOwner: true,
          checkedArtist: true,
        },
      ],
    };

    //this.handleSubmit.bind(this);
    this.setStateForCheckboxes.bind(this);
    //this.handleCheckboxChange.bind(this);
    this.handleInputFirstName = this.handleInputFirstName.bind(this);
    this.handleInputLastName = this.handleInputLastName.bind(this);
    this.handleInputEmail = this.handleInputEmail.bind(this);
  }

 

  setStateForCheckboxes(token, userRoles) {
    if (userRoles.length === 4) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: true,
          checkedUser: true,
          checkedOwner: true,
          checkedArtist: true,
        }],
      };
    } else if (
      userRoles.includes("ADMIN") &&
      userRoles.includes("USER") &&
      userRoles.includes("ARTIST")
    ) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: true,
          checkedUser: true,
          checkedOwner: false,
          checkedArtist: true,
        }],
      };
    } else if (
      userRoles.includes("ADMIN") &&
      userRoles.includes("USER") &&
      userRoles.includes("OWNER")
    ) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: true,
          checkedUser: true,
          checkedOwner: true,
          checkedArtist: false,
        }],
      };
    } else if (
      userRoles.includes("ADMIN") &&
      userRoles.includes("ARTIST") &&
      userRoles.includes("OWNER")
    ) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: true,
          checkedUser: false,
          checkedOwner: true,
          checkedArtist: true,
        }],
      };
    } else if (
      userRoles.includes("USER") &&
      userRoles.includes("ARTIST") &&
      userRoles.includes("OWNER")
    ) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: false,
          checkedUser: true,
          checkedOwner: true,
          checkedArtist: true,
        }],
      };
    } else if (userRoles.includes("ADMIN") && userRoles.includes("ARTIST")) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: true,
          checkedUser: false,
          checkedOwner: false,
          checkedArtist: true,
        }],
      };
    } else if (userRoles.includes("ADMIN") && userRoles.includes("USER")) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: true,
          checkedUser: true,
          checkedOwner: false,
          checkedArtist: false,
        }],
      };
    } else if (userRoles.includes("ADMIN") && userRoles.includes("OWNER")) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: true,
          checkedUser: false,
          checkedOwner: true,
          checkedArtist: false,
        }],
      };
    } else if (userRoles.includes("USER") && userRoles.includes("ARTIST")) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: false,
          checkedUser: true,
          checkedOwner: false,
          checkedArtist: true,
        }],
      };
    } else if (userRoles.includes("USER") && userRoles.includes("OWNER")) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: false,
          checkedUser: true,
          checkedOwner: true,
          checkedArtist: false,
        }],
      };
    } else if (userRoles.includes("ARTIST") && userRoles.includes("OWNER")) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: false,
          checkedUser: false,
          checkedOwner: true,
          checkedArtist: true,
        }],
      };
    } else if (userRoles.includes("ARTIST")) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: false,
          checkedUser: false,
          checkedOwner: false,
          checkedArtist: true,
        }],
      };
    } else if (userRoles.includes("ADMIN")) {
      console.log("aici")
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: true,
          checkedUser: false,
          checkedOwner: false,
          checkedArtist: false,
        }],
      }
      console.log(this.state.checkbox)
    } else if (userRoles.includes("USER")) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: false,
          checkedUser: true,
          checkedOwner: false,
          checkedArtist: false,
        }],
      };
    } else if (userRoles.includes("OWNER")) {
      this.setState = {
        token,
        checkbox: [{
          checkedAdmin: false,
          checkedUser: false,
          checkedOwner: true,
          checkedArtist: false,
        }],
      };
    }
  }
  componentDidMount() {
    UserService.getUserById(this.state.userId).then(res => {
      let user = res.data;
      this.setState({
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        roles: user.roles
      })
    })
    let token = localStorage.getItem("auth-token")
    this.setState({token})
    this.setStateForCheckboxes(this.state.token, this.state.roles)
    console.log(this.state.roles)
  }

  handleSubmit = async (e) => {
    e.preventDefault();
    let roles = [];
    if (this.state.checkbox[0].checkedAdmin === true) {
      roles.push("ADMIN");
    }
    if (this.state.checkbox[0].checkedUser === true) {
      roles.push("USER");
    }
    if (this.state.checkbox[0].checkedOwner === true) {
      roles.push("OWNER");
    }
    if (this.state.checkbox[0].checkedArtist === true) {
      roles.push("ARTIST");
    }

    const updateUser = {
      email: this.state.email,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      roles
    };
    console.log(updateUser)
    console.log(this.state.userId);
    await axios.put(`/admin/async/user/update/${this.state.userId}`, updateUser);

    alert("User has been successfully updated!");
  }

  handleInputChange = (e) => {
    const target = e.target;
    const value = target.value;
    const name = target.name;
    this.setState = {
      [name]: value,
    };
  };

  handleInputFirstName(event){
    this.setState({firstName: event.target.value})
  }
  handleInputLastName(event){
    this.setState({lastName: event.target.value})
  }
  handleInputEmail(event){
    this.setState({email: event.target.value})
  }
  handleRolesChange(event){
    this.setState({roles: event.target.value})
  }

  handleCheckboxChange(event){
    console.log(event.target.name)
    console.log(this.state.checkbox)
    console.log(event.target.checked)
    this.setState({
      checkbox: [
        { 
          ...this.state.checkbox[0],
          [event.target.name]: event.target.checked 
        },
      ],
    });
  };

  render() {
    return (
      <div>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
        ></link>
        <Navbar />
        <br />
        <div className="row">
          <Link className="btn grey button-back" to="/dashboard">
            Back
          </Link>
          <h1>Edit user</h1>
          <form onSubmit={this.handleSubmit.bind(this)}>
            <div className="input-field">
              <input
                type="text"
                value={this.state.firstName}
                name="firstName"
                ref="firstName"
                onChange={this.handleInputFirstName}
              />
              <label htmlFor="firstName">First Name</label>
            </div>
            <div className="input-field">
              <input
                type="text"
                value={this.state.lastName}
                name="lastName"
                ref="lastName"
                onChange={this.handleInputLastName}
              />
              <label htmlFor="lastName">Last Name</label>
            </div>
            <div className="input-field">
              <input
                type="text"
                value={this.state.email}
                name="email"
                ref="email"
                onChange={this.handleInputEmail}
              />
              <label htmlFor="email">Email</label>
            </div>
            <div className="roles-container" style={{ marginBottom: "50px" }}>
              <FormGroup row>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.checkbox[0].checkedAdmin}
                      onChange={this.handleCheckboxChange.bind(this)}
                      name="checkedAdmin"
                      color="primary"
                    />
                  }
                  label="ADMIN"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.checkbox[0].checkedUser}
                      onChange={this.handleCheckboxChange.bind(this)}
                      name="checkedUser"
                      color="primary"
                    />
                  }
                  label="USER"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.checkbox[0].checkedOwner}
                      onChange={this.handleCheckboxChange.bind(this)}
                      name="checkedOwner"
                      color="primary"
                    />
                  }
                  label="OWNER"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.state.checkbox[0].checkedArtist}
                      onChange={this.handleCheckboxChange.bind(this)}
                      name="checkedArtist"
                      color="primary"
                    />
                  }
                  label="ARTIST"
                />
              </FormGroup>
            </div>
            <input type="submit" name="submit" value="Save" className="btn" />
          </form>
        </div>
      </div>
    );
  }
}
