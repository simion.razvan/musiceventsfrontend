import React, { Component } from "react";
import "./pages/AdminDashboard.css";
import { Link } from "react-router-dom";

export default class AdminNavBar extends Component {

    constructor(){
        super();
        this.state = {

        }
        this.handleLogout.bind(this);
    }
    handleLogout = () => {

    }
  render() {
    return (
      <div>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
        ></link>
        <nav className="blue darken-3">
          <div className="nav-wrapper">
            <a href="/dashboard" className="brand-logo">
              Crowder
            </a>
            {/* <a
              data-target="main-menu"
              className="button-collapse show-on-large sidenav-trigger"
            >
              <i className="fa fa-bars"></i>
            </a> */}
            <ul className="right hide-on-small-only">
              <li>
                <Link to="/dashboard">
                  <i className="fa fa-users"></i>Dashboard
                </Link>
              </li>
              <li onClick={this.handleLogout}>
                {/* ATTENTION HERE IF 2 TOKENS WILL BE ADDED if so change the route */}
                <Link to="/login">
                  <i className="fa fa-sign-out"></i>Log out
                </Link>
              </li>
            </ul>
          </div>
          {/*
            <ul className="side-nav" id="main-menu">
              <li className="blue darken-3">
                <Link to="/dashboard">
                  <i className="fa fa-users"></i>Dashboard
                </Link>
              </li>
          </ul> */}
        </nav>
      </div>
    );
  }
}
