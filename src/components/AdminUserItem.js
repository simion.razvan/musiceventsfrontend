import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class AdminUserItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
        user: props.user
    };
  }

  render() {
    return (
      <li className="collection-item">
        <Link to={`/users/${this.state.user.id}`}>{this.state.user.firstName} {this.state.user.lastName}</Link>
      </li>
    );
  }
}
