import React, { useState, useEffect } from "react";
import NavBar from "../NavBar";
import Footer from "../reusables/Footer";
import FormSubmit from "./FormSubmit";
import "./SubmitEvent.css";
import VenuesService from "../../services/VenuesService";
import Axios from "axios";

export default class SubmitEvent extends React.Component {
  //const { userData, setUserData } = useContext(UserContext);
  constructor(){
      super();
      this.state = {
          loggedIn: undefined,
          venues: []
      }
      this.checkLoggedIn.bind(this);
      this.getVenues.bind(this);
  }
  checkLoggedIn = () => {
    let token = localStorage.getItem("auth-token");
    if (token === "") {
      return false;
    }
    return true;
  };
  getVenues = async () => {
    await Axios.get("/venues",{
      headers:{
        "Content-Type":"application/json",
      }
    })
    .then((response) =>{
      this.setState({venues: response.data})
    })
    .catch(err => console.log(err))
  }

  async componentDidMount() {
    this.setState({
      loggedIn: this.checkLoggedIn(),
    });
    await this.getVenues();
    //console.log(this.state.venues)
  }
  render() {
    return (
      <div className="page">
        <div className="header">
          <div className="row">
            <div className="navbar">
              <NavBar />
            </div>
            <div className="header-title">
              <div className="manage-events">
                {this.state.loggedIn ? <a href="/user-events">My Events</a> : null} 
                {/*<a href="/user-events">My Events</a> */}
              </div>
              <h1>Submit Event</h1>
            </div>
          </div>
        </div>
        {this.state.loggedIn ? (
          <FormSubmit venues={this.state.venues}/>
        ) : (
          <div className="row below-header">
            <p>You need to be logged in to be able to submit an event.</p>
            <br></br>
            <a href="/register">Click here to Register </a>
            or
            <a href="/login"> Login</a>
          </div>
        )}
        <Footer />
      </div>
    );
  }
}
