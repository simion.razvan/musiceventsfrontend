import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import EventAvailableRoundedIcon from "@material-ui/icons/EventAvailableRounded";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Select from "@material-ui/core/Select";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { setDate } from "date-fns";
import jwt from "jwt-decode";
import axios from "axios";
import VenuesService from "../../services/VenuesService";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  dropdown: {
    width: "100%",
  },
  timePicker: {
    width: "100%",
  },
  datePicker: {
    width: "100%",
  },
}));
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 400,
    },
  },
};
function FormSubmit(props) {
  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [date, setDate] = useState();
  const [time, setTime] = useState();
  const [venueName, setVenue] = useState();
  //const [venueId, setVenueId] = useState();

  const history = useHistory();


  const handleSubmit = async (e) => {
    e.preventDefault();
    let token = localStorage.getItem("auth-token");
    let user = jwt(token);
    let userRole = user.role;
    if (!userRole.includes("OWNER")) {
      alert("You are not an owner!");
      return;
    }
    let data = {
      date,
      description,
      time: time.toString().slice(16, 21), 
      name,
      venueName
    }
    console.log(data)
    await axios({
      method: 'post',
      url: '/event',
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json",
      },
      data
    })

    alert("Event has been successfully created!");
    history.push("/");
  };


  const formatDate = (date) => {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  };
  const handleDateChange = (date) => {
    setDate(formatDate(date));
    console.log(formatDate(date));
  };

  const handleTimeChange = (time) => {
    setTime(time);
  };

  const handleVenueChange = (e) => {
    setVenue(e.target.value)
  };

  const classes = useStyles();
  return (
    <div className="row below-header">
      <Container component="main" maxWidth="xs">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <EventAvailableRoundedIcon />
          </Avatar>
          <form className={classes.form} onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12}>
                <TextField
                  autoComplete="fname"
                  name="eventName"
                  variant="outlined"
                  required
                  fullWidth
                  id="firstName"
                  label="Title"
                  autoFocus
                  onChange={(e) => setName(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="email"
                  label="Description"
                  name="description"
                  autoComplete="description"
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Grid>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid item xs={12}>
                  <KeyboardDatePicker
                    //disableToolbar
                    className={classes.datePicker}
                    format="yyy-MM-dd"
                    margin="normal"
                    id="date-picker-inline"
                    label="Date picker inline"
                    value={date}
                    onChange={handleDateChange}
                    KeyboardButtonProps={{
                      "aria-label": "change date",
                    }}
                  />
                </Grid>
              </MuiPickersUtilsProvider>
              <Grid item xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardTimePicker
                    className={classes.timePicker}
                    margin="normal"
                    id="time-picker"
                    label="Time picker"
                    value={time}
                    onChange={handleTimeChange}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item xs={12}>
                <InputLabel id="demo-mutiple-name-label">Venue</InputLabel>
                <Select
                  className={classes.dropdown}
                  labelId="demo-mutiple-name-label"
                  id="demo-mutiple-name"
                  value={venueName}
                  onChange={handleVenueChange}
                  input={<Input />}
                  MenuProps={MenuProps}
                >
                  {props.venues.map((venue, index) => {
                    return(
                      <MenuItem value={venue.name}>{venue.name}</MenuItem>
                    );
                  })}
                </Select>
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Create
            </Button>
          </form>
        </div>
      </Container>
    </div>
  );
}

export default FormSubmit;
