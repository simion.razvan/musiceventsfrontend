import React from 'react';
import image from "../img/artist.jpg";

function Artist(props) {
    return (
        <li className="min-height-small">
            <article className="highlight-top">
                <a href="/artist/{id}">
                    <img className="nohide" src={image}></img>
                </a>
                {/*add artist name from passed props */}
                <a href="/artist/{id}">{props.artistsData.firstName}</a>
                {/*add date when joined */}
                <p className="joined">Member since: 09 Dec 2020</p>
            </article>
        </li>
    )
}

export default Artist
