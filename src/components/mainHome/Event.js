import React from 'react'
import "./Main.css";
import image from "../img/event_cover.jpg";

function Event(props) {
    const formatDate = (date) => {
        return date.dayName + ", " + date.day + "-" + date.month + "-" + date.year;
    }

    return (
        <li className="min-height-small">
            <article className="highlight-top">
                <p className="date">{formatDate(props.date)}</p>
                <a href="/event/{id}">
                    <img className="nohide" src={image}></img>
                </a>
                <p className="counter nohide">
                    <span>12</span>
                    attending
                </p>
                <a href="/event/{id}">
                    <h1>{props.eventsData.name}</h1>
                </a>
                <p className="copy nohide">
                    <a href="/venue/{id}">{props.eventsData.venue.location}</a>
                </p>
            </article>
        </li>
    )
}

export default Event
