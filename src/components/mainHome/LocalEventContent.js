import React, {useContext} from "react";
import "./Main.css";
import Event from "./Event.js";
import BtnSubmitEvent from "../reusables/BtnSubmitEvent.js";
import UserContext from "../../context/UserContext";

export default function HomeEventContent(props) {
  const{ userData, setUserData } = useContext(UserContext);
  return (
    <section id="events" className="row content clearfix areaEvents">
      <h1 className="heading listing-heading small">Popular events</h1>
      <div className="but heading-more mobile-off">
        <a href="/events">More Events</a>
      </div>
      <div id="events-listing" className="strip small slide">
        <ul className="list small clearfix popular">
          {props.events.map((event) => (
            <Event key={event.id} date={props.date} eventsData={event} id={event.id}/>
          ))}
        </ul>
        <BtnSubmitEvent loggedIn={userData.loggedIn}/>
      </div>
    </section>
  );
}
