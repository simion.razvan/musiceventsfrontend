import React from 'react'
import Artist from "./Artist.js";
import BtnBecomeMember from "../reusables/BtnBecomeMember.js";

function LocalArtistContent(props) {
    return (
        <section className="row content clearfix areaArtists">
            <h1 className="heading listing-heading small">Discover artists</h1>
            <div className="but heading-more mobile-off">
                <a href="/artists">More Artists</a>
            </div>
            <div id="artists-listing" className="strip small slide">
                <ul className="list small clearfix popular">
                    {props.artists.map((artist) =>(
                        <Artist key={artist.id} artistsData={artist} id={artist.id}/>
                    ))}
                </ul>
                <BtnBecomeMember/>
            </div>
        </section>
    )
}

export default LocalArtistContent
