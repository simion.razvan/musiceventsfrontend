import React, { Component } from "react";
import NavMain from "./NavMain";
import "./Main.css";
import LocalEventContent from "./LocalEventContent.js"
import LocalArtistContent from "./LocalArtistContent.js";
import Footer from "../reusables/Footer.js";
import EventsService from "../../services/EventsService";
import ArtistService from "../../services/ArtistService";

export default class Main extends Component {
  constructor() {  
    super();
    let startDate = new Date();
    let fetchDate = this.formatDate(startDate);
    let firstDate = this.formatDate(fetchDate);
    this.state = {
      date: [
        {
          day: 0,
          month: 0,
          year: 0,
          dayName: "",
        },
        {
          day: 0,
          month: 0,
          year: 0,
          dayName: "",
        },
        {
          day: 0,
          month: 0,
          year: 0,
          dayName: "",
        },
        {
          day: 0,
          month: 0,
          year: 0,
          dayName: "",
        },
        {
          day: 0,
          month: 0,
          year: 0,
          dayName: "",
        },
      ],
      events:[],
      artists:[],
      currentSelectedIndex: 0,
      selectedDate: undefined
    };
    this.GetOneDayFormat.bind(this);
    this.SetDate.bind(this);
    this.formatDate.bind(this);
    console.log(this.state.selectedDate)
    console.log(firstDate)
  }
  
  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}
  componentDidMount() {
    let startDate = new Date();
    let fetchDate = this.formatDate(startDate);
    EventsService.getEventsByDate(fetchDate).then((res) => {
      this.setState({
        events: res.data,
      })
    }) 
    ArtistService.getArtists().then((res) => {
      this.setState({artists: res.data});
    })
    this.SetDate(startDate);
    console.log(this.state.date);
  }

  SetDate(startDate) {
    let dates = this.state.date;
    for (let i = 0; i < 5; i++) {
      let currentDate = new Date();
      currentDate.setDate(startDate.getDate() + i);
      dates[i].day = String(currentDate.getDate());
      dates[i].month = String(currentDate.getMonth() + 1);
      dates[i].year = String(currentDate.getFullYear());
      dates[i].dayName = String(this.GetOneDayFormat(currentDate));
    }

    this.setState({
      date: dates,
      selectedDate: this.state.date[0]
    });
  }

  GetOneDayFormat = (today) => {
    let dayNumber = today.getDay();
    let dayName = "";
    switch (dayNumber) {
      case 0:
        dayName = "Sunday".substring(0, 3);
        break;
      case 1:
        dayName = "Monday".substring(0, 3);
        break;
      case 2:
        dayName = "Tuesday".substring(0, 3);
        break;
      case 3:
        dayName = "Wednesday".substring(0, 3);
        break;
      case 4:
        dayName = "Thursday".substring(0, 3);
        break;
      case 5:
        dayName = "Friday".substring(0, 3);
        break;
      case 6:
        dayName = "Saturday".substring(0, 3);
        break;
      default:
        break;
    }
    return dayName;
  };

  render() {
    return (
      <>
      <main id="main">
        <ul id="pnlMain" className="content-list home-list">
          <li id="pnlEvents">
            <ul className="events events-border">
              <li id="liEventDatePicker">
                <NavMain 
                data={this.state.date}
                fetchEvents={(events, data) => this.setState({
                  events,
                  selectedDate: data
                })}
                currentFetchDate={this.state.currentFetchDate}
                events={this.state.events}
                />
              </li>
              <li id="liLocalEvents" className="mobile-off">
                <LocalEventContent date={this.state.selectedDate} events={this.state.events}/>
              </li>
            </ul>
          </li>
          <li id="pnlArtists">
            <ul className="artists artist-border">
              <li className="nobreak">
                <nav id="sectionNav" className="datepicker artist">
                  <div className="clearfix">
                    <h1>Artists</h1>
                  </div>
                </nav>
              </li>
              <li>
                <LocalArtistContent artists={this.state.artists}/>
              </li>
            </ul>
          </li>
        </ul>
      </main>
      <Footer/>
      </>
    );
  }
}
