import React, { Component } from "react";
import "./Main.css";
import NavMainDateEvent from "./NavMainDateEvent";
import EventsService from "../../services/EventsService";

export default class NavMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSelectedIndex: 0,
      dates: props.data,
      events: props.events
    }
    this.handleSelect.bind(this);
    this.formatDate.bind(this);
  }
  formatDate(date) {
    let month = date.month;
    let day = date.day;
    let year = date.year;

   if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    return [year, month, day].join('-');
}

  handleSelect = (index, data, normalData) => {
    console.log(normalData);
    this.setState({currentSelectedIndex: index})
    EventsService.getEventsByDate(data).then((res) => {
      this.props.fetchEvents(res.data, normalData)
    })

    console.log(this.state.events);
  }

//remove all logic from here and add it in the parent comp "Main.js"

  render() {
    return (
      <nav id="sectionNav" className="datepicker event">
        <div className="clearfix list-days">
          <h1>Events</h1>
          <ul className="mobile-off">
            {this.state.dates.map((data, index) => (
              <NavMainDateEvent 
                onClick={() => this.handleSelect(index, this.formatDate(data), data)}
                name={this.state.currentSelectedIndex === index && "selected"}
                dataDate={data}
                dayName={data.dayName}
                dayDate={data.day}
              /> 
            ))}
          </ul>
        </div>
      </nav>
    );
  }
}
