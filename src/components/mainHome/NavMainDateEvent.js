import React, {Component} from 'react';

export default class NavMainDateEvent extends Component{

    render(){
        return(
            <li
              onClick={this.props.onClick}
              className={this.props.name}
              data-date={this.props.dataDate}
            >
              <a href="#">
                {this.props.dayName}
                <h1>{this.props.dayDate}</h1>
              </a>
            </li>   
        )
    }
}