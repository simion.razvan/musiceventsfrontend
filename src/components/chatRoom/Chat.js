import React, { useState, useEffect, useContext } from "react";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import NavBar from "../NavBar";
import Footer from "../reusables/Footer";
import User from "./EndUser";
import "./Chat.css";
import SendMessageForm from "./SendMessageForm";
import MessageList from "./MessageList";
import ArtistService from "../../services/ArtistService";
import OwnerService from "../../services/OwnerService";
import jwt from "jwt-decode";

const client = new W3CWebSocket("ws://localhost:9090/ws/chat");
export default class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: undefined,
      messages: [],
      availableUsers: {
        artists: [],
        owners: [],
      },
      displayEndUser: false,
      userToShow: undefined
    };
    this.sendMessage = this.sendMessage.bind(this);
    this.retrieveUsers = this.retrieveUsers.bind(this);
    this.getUsername = this.getUsername.bind(this);
    this.onUserClick = this.onUserClick.bind(this);
  }

  componentDidMount() {
    client.onopen = () => {
      console.log("WebSocket client connected");
    };
    client.onmessage = (message) => {
      const dataFromServer = JSON.parse(message.data);
      if (dataFromServer.type === "message") {
        this.setState({
          messages: [
            ...this.state.messages,
            {
              msg: dataFromServer.msg,
              user: dataFromServer.user,
            },
          ],
        });
      }
    };
    this.retrieveUsers();
    this.getUsername();
  }
  getUsername = () => {
    let token = localStorage.getItem("auth-token");
    if (token === "") {
      this.setState({ userName: undefined });
    }
    const user = jwt(token);
    const username = user.sub;
    this.setState({ userName: username });
    console.log(this.state.userName);
  };

  retrieveUsers = async () => {
    let artists = [];
    await ArtistService.getArtists().then((res) => {
      artists = res.data;
      let index = -1;
      for (let i = 0; i < artists.length; i++) {
        if (artists[i].username === this.state.userName) {
          index = i;
        }
      }
      if (index > -1) {
        artists.splice(index, 1);
      }
    });
    //if you are an artist or owner
    //do not show yourself in the list of the available users
    let owners = [];
    await OwnerService.getOwners().then((res) => {
      owners = res.data;
      let index2 = -1;
      for (let i = 0; i < owners.length; i++) {
        if (owners[i].username === this.state.userName) {
          index2 = i;
        }
      }
      if (index2 > -1) {
        owners.splice(index2, 1);
      }
    });
    this.setState({
      availableUsers: {
        artists,
        owners
      },
    });
  };


  sendMessage(msg) {
    client.send(
      JSON.stringify({
        type: "message",
        msg,
        user: this.state.userName,
      })
    );
  }
  onUserClick = (id) => {
    let artistToShow = undefined;
    let ownerToShow = undefined;
    this.state.availableUsers.artists.forEach(artist => {
      if(artist.id === id){
        artistToShow = artist;
        console.log(artistToShow)
      }
    })
    if(artistToShow === undefined){
      this.state.availableUsers.owners.forEach(owner => {
        if(owner.id === id){
          ownerToShow = owner;
        }
      })
    }
    if(artistToShow === undefined){
      this.setState({
        displayEndUser: !this.state.displayQuestions,
        userToShow: ownerToShow,
        messages: []
      })
    }else{
      this.setState({
        displayEndUser: !this.state.displayQuestions,
        userToShow: artistToShow,
        messages: []
      })
    }
  }

  render() {
    let userToShow;
    let USER;
    if(this.state.displayEndUser){
      userToShow = this.state.userToShow;
      USER = <User
      key={userToShow.id}
      fName={userToShow.firstName}
      lName={userToShow.lastName}
      
    />
    }
    return (
      <>
        <div className="chat">
          <div className="row">
            <div className="navbar">
              <NavBar />
            </div>

            <div className="chat-container">
              <div className="chat-header">
                <h1>Chat Room</h1>
              </div>
              <div className="chat-panel-container">
                <div className="chat-users">
                  <div className="headline-users">
                    <h1>Available Users</h1>
                  </div>
                  <div className="available-container">
                    <div className="available artist-user">
                      <h1 className="title-available">Artists</h1>
                      <div className="users">
                        <ul>
                          {this.state.availableUsers.artists.map((artist) => (
                            <li onClick={() => this.onUserClick(artist.id)}>
                              <User
                                key={artist.id}
                                fName={artist.firstName}
                                lName={artist.lastName}
                                
                              />
                            </li>
                          ))}
                        </ul>
                      </div>
                    </div>
                    <div className="available owner-user">
                      <h1 className="title-available">Owners</h1>
                      <div className="users">
                        <ul>
                          {this.state.availableUsers.owners.map((owner) => (
                            <li onClick={() => this.onUserClick(owner.id)}>
                              <User
                                key={owner.id}
                                fName={owner.firstName}
                                lName={owner.lastName}
                              />
                            </li>
                          ))}
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="chat-box">
                  <div className="headline-box">
                    {/* pass props for the chosen user to chat with*/}
                    {USER}
                  </div>
                  <div className="chat-msg-container">
                    <MessageList
                      messages={this.state.messages}
                      user={this.state.messages}
                    />
                  </div>
                  <SendMessageForm sendMessage={this.sendMessage} />
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </>
    );
  }
}
