import React from "react";
import ImageAvatar from "./ImageAvatar";
import "./Chat.css";

function EndUser(props) {
  return (
    <div className="user-container">
      <ImageAvatar />
      <h1>{props.fName} {props.lName}</h1>
    </div>
  );
}

export default EndUser;
