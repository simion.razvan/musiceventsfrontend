import React from "react";
import "../../App.css";
import "./HeroSection.css";
import NavBar from "../NavBar";
import HeroTextBox from "./HeroTextBox";

function HeroSection() {
  return (
    <header className="header-home" id="home">
      <div className="row">
        <NavBar />
        <HeroTextBox />
      </div>
    </header>
  );
}

export default HeroSection;
