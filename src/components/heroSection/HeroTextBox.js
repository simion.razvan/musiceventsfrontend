import { LineStyle } from "@material-ui/icons";
import React from "react";
import { Link } from "react-router-dom";
import "../../App.css";
import { Button } from "../Button";
import "./HeroSection.css";

function HeroTextBox() {
  return (
    <div className="hero-text-box">
      <h1>ADVENTURE AWAITS</h1>
      <p>What are you waiting for?</p>
      <div className="hero-btns">
        <Link to="/register">
          <Button
            className="btn"
            id="btn-getstarted"
            buttonStyle="btn--outline"
            buttonSize="btn--large"
          >
            GET STARTED
          </Button>
        </Link>
      </div>
    </div>
  );
}

export default HeroTextBox;
