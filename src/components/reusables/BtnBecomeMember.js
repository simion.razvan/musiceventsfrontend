import React from 'react'

function BtnBecomeMember() {
    return (
        <div className="but myra artist">
            <a className="become-member" href="/artist/join">Join artist community</a>
        </div>
    )
}

export default BtnBecomeMember
