import React from "react";
import "../mainHome/Main.css";
import logo from "../img/logo.png";
import BtnMember from "../reusables/BtnBecomeMember.js";
import BtnEvent from "../reusables/BtnSubmitEvent.js";

function Footer() {
  return (
    <footer className="footer clearfix">
      <ul className="row content-list">
        <li>
          <div className="content clearfix">
            <div className="clearfix plus8">
              <div className="fl col2-3">
                <ul className="pb32">
                  <li id="logo-footer">
                    <a href="/">
                      <img className="logo" src={logo}></img>
                    </a>
                  </li>
                  <li className="f12 grey pt16">Copyright 2020 Crowder</li>
                  <li className="f12 grey">All rights reserved.</li>
                  <li className="f12 grey">
                    <a href="/privacy">Privacy</a>&<a href="/terms">Terms</a>
                  </li>
                </ul>
              </div>
              <div className="fl col4-5 pt60">
                <div className="fl col1 footer-category">
                  <ul>
                    <li>
                      <a href="/about">About</a>
                    </li>
                    <li>
                      <a href="/advertise">Advertise</a>
                    </li>
                    <li>
                      <a href="/venues">Venues</a>
                    </li>
                    <li id="btnMemberFooter"><BtnMember/></li>
                    <li><BtnEvent/></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </footer>
  );
}

export default Footer;
