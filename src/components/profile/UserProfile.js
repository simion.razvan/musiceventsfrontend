import React from "react";
import NavBar from "../NavBar";
import Footer from "../reusables/Footer";
import "./UserProfile.css";
import UpdatePassword from "./UpdatePassword";
import jwt from "jwt-decode";
import axios from "axios";

export default class UserProfile extends React.Component {

  constructor(props){
      super(props);
      this.state = {
        userId: '',
        userData: ''
      }
    this.handleModal.bind(this);
    this.handleClose.bind(this);
    this.getUserById.bind(this);
  }
  getUserById = () =>{
    let id = this.props.match.params.id;
    console.log(id)
    axios.get(`/user/${id}`,{
      headers:{
        "Content-Type":"application/json",
      }
    })
    .then((res) => {
      this.setState({userData: res.data})
    })
    .catch(err => console.log(err))
  }
  componentWillMount(){
    let token = localStorage.getItem("auth-token");
    if(token !== ""){
      let user = jwt(token)
      this.setState({userId: user.id})
    }
    this.getUserById();
  }
  handleModal(){
    document.querySelector('.bg-modal').style.display = 'flex';
  }
  handleClose(){
    document.querySelector('.bg-modal').style.display = 'none';
  }
  
  
  render() {
    const roleItems = 
      this.state.userData 
      && this.state.userData.roles
      && this.state.userData.roles.length > 0 
      && this.state.userData.roles.map(
        (role) => {
          return(
            <li>{role.name}; </li>
          )
        }
      ) 
    return (
      <div className="profile-page">
        <div className="header">
          <div className="row">
            <div className="navbar">
              <NavBar />
            </div>
            <div className="header-title">
              <h1>Profile</h1>
            </div>
          </div>
        </div>
        <div className="row below-header">
          <div className="fields name-title">
            <h1>{this.state.userData.firstName} {this.state.userData.lastName}</h1>
          </div>
          <div className="fields email">
            <ul className="list-fields">
              <div className="grouped">
                <li className="field-title">Email: </li>
                <li>{this.state.userData.email}</li>
              </div>
              <div className="grouped">
                <li className="field-title">Role: </li>
                {roleItems}
              </div>
              <div className="grouped password">
                <li 
                className="field-title"
                onClick={this.handleModal}
                >Change password</li>
              </div>
              <div 
              className="bg-modal">
                <div className="modal-content">
                    <div className="close"
                    onClick={this.handleClose}>+</div>
                    <UpdatePassword userId={this.state.userId}/>
                </div>
              </div>
            </ul>
          </div>
        </div>
        <Footer/>
      </div>
    );
  }
}
