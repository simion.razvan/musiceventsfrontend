import React, { Component } from "react";
import "./AdminDashboard.css";
import "../AdminNavBar";
import Navbar from "../AdminNavBar";
import { Link } from "react-router-dom";
import axios from "axios";
import AdminUserItem from "../AdminUserItem";

export default class AdminDashboard extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
    };
    this.getUsers.bind(this);
  }
  componentWillMount() {
    this.getUsers();
  }
  getUsers = async () => {
    let token = localStorage.getItem("auth-token");
    if (token !== "") {
      await axios
        .get("/admin/users/async", {
          headers: {
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json",
          },
        })
        .then((response) => {
          this.setState({ users: response.data });
        })
        .catch(err => console.log(err));
    }
  };
  render() {
    const userItems = this.state.users.map((user, index) => {
      return (
          <AdminUserItem key={user.id} user={user} />
      );
    });
    return (
      <div className="dashboard">
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
        ></link>
        <Navbar />
        <div className="row">
          <h1>Dashboard</h1>
          <ul className="collection">
              {userItems}
          </ul>
        </div>
      </div>
    );
  }
}
