import React from "react";
import NavBar from "../NavBar";
import Footer from "../reusables/Footer";
import "./ArtistJoin.css";

export default class ArtistJoin extends React.Component {

    constructor(){
        super();
        this.state = {

        }
        this.handleSubmit.bind(this);
    }
  handleSubmit = async (e) => {
    e.preventDefault();
  }
  render() {
    return (
      <div className="join-artist">
        <div className="header">
          <div className="row">
            <div className="navbar">
              <NavBar />
            </div>
            <div className="header-title">
              <h1>Join artist community</h1>
            </div>
          </div>
        </div>
        <div className="row below-header">
            <p>Send a request by clicking this button and become an artist</p>
            <br></br>
            
            <button id="btnJoinArtist"onClick={this.handleSubmit}>Join artist community</button>
        </div>
        <Footer/>
      </div>
    );
  }
}
