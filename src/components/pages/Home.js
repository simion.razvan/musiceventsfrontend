import React from 'react';
import "../../App.css";
import HeroSection from "../heroSection/HeroSection";
import Main from "../mainHome/Main";

function Home() {
  return (
    <>
      <HeroSection />
      <Main />
    </>
  );
}

export default Home;
