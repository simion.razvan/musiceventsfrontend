import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Navbar from "./AdminNavBar";

export default class AdminUserDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      details: "",
      token: "",
    };
    this.getUserById.bind(this);
    this.handleDelete.bind(this);
  }

  componentWillMount() {
    this.getUserById();
  }
  handleDelete = async () => {
    let userId = this.state.details.id;
    axios
      .delete(`/admin/async/delete/user/${userId}`, {
        headers: {
          Authorization: "Bearer " + this.state.token,
          "Content-Type": "application/json",
        },
      })
      .then((response) => {
        this.props.history.push("/dashboard");
      })
      .catch((err) => console.log(err));
  };

  getUserById = async () => {
    let token = localStorage.getItem("auth-token");
    let userId = this.props.match.params.id;
    if (token !== "") {
      await axios
        .get(`/user/${userId}`, {
          headers: {
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json",
          },
        })
        .then((response) => {
          this.setState({
            details: response.data,
            token,
          });
        })
        .catch((err) => console.log(err));
    }
  };

  render() {
    return (
      <div>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
        ></link>
        <Navbar />
        <br></br>
        <div className="row">
          <Link className="btn grey" to="/dashboard">
            Back
          </Link>
          <h1>
            {this.state.details.firstName} {this.state.details.lastName}
          </h1>
          <ul className="collection">
            <li className="collection-item">
              Username: {this.state.details.username}
            </li>
            <li className="collection-item">
              Email: {this.state.details.email}
            </li>
            {/*{this.state.details.roles.map((role, index) => {
                <li className="collection-item">Role: {role}</li>
            });
            } */}
          </ul>
          <Link
            className="btn"
            to={`/users/edit/${this.state.details.id}`}  
          >
            Edit
          </Link>
          {/* try to add onClick method on a button and use push.history for sending props*/}
          <button onClick={this.handleDelete} className="btn red right">
            Delete
          </button>
        </div>
      </div>
    );
  }
}
