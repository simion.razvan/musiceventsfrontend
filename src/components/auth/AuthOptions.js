import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import { Button } from "../Button";
import UserContext from "../../context/UserContext";

export default function AuthOptions() {
  const { userData, setUserData } = useContext(UserContext);
  const history = useHistory();

  const register = () => history.push("/register");
  const login = () => history.push("/login");
  const logout = () => {
    setUserData({
      token: undefined,
      user: undefined,
      username: undefined,
      userRole: undefined,
      loggedIn: false
    });
    history.push("/");
    localStorage.setItem("auth-token", "");
  };
  const profile = () => {
    let userId = userData.user.id;
    history.push(`/profile/${userId}`);
  }
  const chat = () => history.push("/chat");
  if (userData.user) {
    if (userData.userRole) {
      if (
        userData.userRole.includes("ARTIST") ||
        userData.userRole.includes("OWNER")
      ) {
        return (
          <div className="auth-options">
            <>
              <li id="loginDrop">
                <Button
                  className="btn"
                  buttonStyle="btn--primary"
                  buttonSize="btn--medium"
                  onClick={profile}
                >
                  Profile
                </Button>
                </li>
                <li id="loginDrop">
                <Button
                  className="btn"
                  buttonStyle="btn--primary"
                  buttonSize="btn--medium"
                  onClick={chat}
                >
                  Chat
                </Button>
                </li>
              <li id="loginDrop">
                <Button
                  className="btn"
                  buttonStyle="btn--primary"
                  buttonSize="btn--medium"
                  onClick={logout}
                >
                  Logout
                </Button>
              </li>
            </>
          </div>
        );
      }
    }
    return (
      <div className="auth-options">
        <div>
          <li id="loginDrop">
            <Button
              className="btn"
              buttonStyle="btn--primary"
              buttonSize="btn--medium"
              onClick={profile}
            >
              Profile
            </Button>
          </li>
          <li id="loginDrop">
            <Button
              className="btn"
              buttonStyle="btn--primary"
              buttonSize="btn--medium"
              onClick={logout}
            >
              Logout
            </Button>
          </li>
        </div>
      </div>
    );
  }
  if (!userData.user) {
    return (
      <>
        <li id="loginDrop">
          <Button
            className="btn"
            buttonStyle="btn--primary"
            buttonSize="btn--medium"
            onClick={login}
          >
            Login
          </Button>
        </li>
        <li>
          <Button
            className="btn"
            buttonStyle="btn--primary"
            buttonSize="btn--medium"
            onClick={register}
          >
            Register
          </Button>
        </li>
      </>
    );
  }
}
