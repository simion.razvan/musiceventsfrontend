import React, { useState, useEffect } from "react";
import { Button } from "./Button";
import { Link } from "react-router-dom";
import "../App.css";
import "./NavBar.css";
import DropMenu from "./DropMenu";
import logo from "./img/logo.png";

function Navbar() {
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener("resize", showButton);

  return (
    <>
      <nav className="navbar">
        <div className="row">
          <div className="navbar-container">
            <Link to="/" className="navbar-logo" onClick={closeMobileMenu}>
              <div className="logo-container">
                <img className="logo" alt="logo" src={logo}></img>
              </div>
            </Link>
            <div className="menu-icon" onClick={handleClick}>
              <i className={click ? "fas fa-times" : "fas fa-bars"} />
            </div>
            <ul className={click ? "nav-menu active" : "nav-menu"}>
              <li className="nav-item">
                <Link to="/" className="nav-links" onClick={closeMobileMenu}>
                  Home
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  to="/events"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Events
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  to="/artists"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Artists
                </Link>
              </li>
            </ul>
            {button && <DropMenu />}
          </div>
        </div>
      </nav>
    </>
  );
}

export default Navbar;
