import axios from 'axios';


class UserService{

    getUserById(userId){
        return axios.get(`/user/${userId}`);
    }

    updateUserById(userId, newUser, token){
        return axios.request({
            method: "put",
            url: `/admin/user/update/${userId}`,
            headers:{
                "Authorization": "Bearer " + token,
                "Content-Type": "application/json",
            },
            newUser,
        })
    }
}

export default new UserService();