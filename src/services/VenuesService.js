import axios from 'axios';

class VenuesService{
    getVenues(){
        return axios.get("/venues");
    }
}

export default new VenuesService();