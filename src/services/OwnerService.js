import axios from 'axios';

class OwnerService{

    getOwners(){
        return axios.get("/owners");
    }
}

export default new OwnerService();