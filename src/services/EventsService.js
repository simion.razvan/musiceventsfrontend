import axios from 'axios';

class EventsService{

    getEvents(){
        return axios.get("/events");
    }
    getEventsByDate(date){
        return axios.get("/events/" + date);
    }
}

export default new EventsService();
