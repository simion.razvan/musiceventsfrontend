import axios from 'axios';

//const ARTIST_API_BASE_URL = "https://localhost:9090/artists";

class ArtistService{

    getArtists(){
        return axios.get("/artists");
    }
}


//export directly object of this class
export default new ArtistService();