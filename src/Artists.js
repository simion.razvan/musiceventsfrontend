import React, { Component } from "react";
import NavBar from "./components/NavBar";
import "./App.css";
import "./components/heroSection/HeroSection.css";
import ArtistService from "./services/ArtistService";

export default class Artists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      artists: [],
    };
  }

  componentDidMount() {
    ArtistService.getArtists().then((res) => {
      this.setState({ artists: res.data, loading: false });
      console.log(this.state.artists);
    });
  }

  render() {
    return (
      <div className="row">
        <NavBar />
        <div>
          {this.state.loading || !this.state.artists ? (
            <div>loading...</div>
          ) : (
            <div>
              <h1 className="text-center"> Artist List</h1>
              <table className="table table-striped">
                <thead>
                  <tr>
                    <td>Email</td>
                    <td>First Name</td>
                    <td>Last Name</td>
                    <td>Username</td>
                  </tr>
                </thead>
                <tbody>
                  {this.state.artists.map((artist) => (
                    <tr key={artist.id}>
                      <td> {artist.email} </td>
                      <td> {artist.firstName} </td>
                      <td> {artist.lastName} </td>
                      <td> {artist.username} </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )}
        </div>
      </div>
    );
  }
}
