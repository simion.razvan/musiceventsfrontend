import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Home from "./components/pages/Home";
import Artists from "./Artists";
import Login from "./components/auth/Login";
import SignUp from "./components/auth/Register";
import Chat from "./components/chatRoom/Chat";
import SubmitEvent from "./components/submitEvent/SubmitEvent";
import UserProfile from "./components/profile/UserProfile";
import ArtistJoin from "./components/pages/ArtistJoin";
import AdminDashboard from "./components/pages/AdminDashboard";
import UserContext from "./context/UserContext";
import AdminUserDetails from "./components/AdminUserDetails";
import AdminEditUser from "./components/AdminEditUser";
import Axios from "axios";
import jwt from "jwt-decode";

function App() {
  const [userData, setUserData] = useState({
    token: undefined,
    user: undefined,
    username: undefined,
    userRole: undefined,
    loggedIn: false
  });

  const checkLoggedIn = async () => {
    let token = localStorage.getItem("auth-token");
    if (token === "") {
      localStorage.setItem("auth-token", "");
      token = "";
    }
    const tokenRes = await Axios.post("/profiles/tokenIsValid", null, {
      headers: {
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json",
      },
    });
    if (tokenRes.data) {
      const userRes = await Axios.get("/profiles/user", {
        headers: {
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
        },
      });
      /* get the user role and store it in userData*/
      const user = jwt(token);
      const role = user.role;
      const username = user.sub;
      setUserData({
        token,
        user: userRes.data,
        username: username,
        userRole: role,
        loggedIn: true
      });
    }
  };
  useEffect(() => { 
    checkLoggedIn();
  },[userData.loggedIn]);
  const getUserId = () =>{
    let token = localStorage.getItem("auth-token");
    let id = '';
    if(token !== ""){
      let user = jwt(token)
      id = user.id;
    }
    return id
  }
  return (
    <>
      <Router>
        <UserContext.Provider value={{ userData, setUserData }}>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/artists" component={Artists} />
            <Route exact path="/chat" render={(props) => (<Chat {...props} userData={userData}/>)} />
            <Route exact path="/event-submit" component={SubmitEvent} /> 
            <Route exact path={`/profile/:id`} component={UserProfile}/>
            <Route path="/login" component={Login} />
            <Route path="/register" component={SignUp} />
            <Route exact path="/artist/join" component={ArtistJoin}/>
            {/* userData.userRole.includes("ADMIN") ? <Route exact path="/dashboard" component={AdminDashboard}/> : <h1 style={{color: "#000"}}>404 internal error. Page not found</h1> */} 
            
            <Route exact path="/dashboard" component={AdminDashboard}/>
            <Route exact path="/users/:id" component={AdminUserDetails}/>
            <Route exact path="/users/edit/:id" component={AdminEditUser}/>
          </Switch>
        </UserContext.Provider>
      </Router>
    </>
  );
}

export default App;
